generate "provider" {
  path      = "provider.tf"
  if_exists = "skip"
  contents = <<EOF
provider "google" {
  project = var.project_id
  version = "3.75"
}

provider "google-beta" {
  project = var.project_id
  version = "3.75"
}

EOF
}

generate "backend" {
  path      = "backend.tf"
  if_exists = "overwrite"
  contents = <<EOF
terraform {
  backend "local" {}
}
EOF
}

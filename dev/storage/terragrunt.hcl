terraform {
    source = "github.com/terraform-google-modules/terraform-google-cloud-storage.git//?ref=v2.0.0"
}

include {
  path = find_in_parent_folders()
}

locals {
  this = yamldecode(file("this.yaml"))
}


inputs = {
    prefix = local.this.prefix
    names = local.this.names
    project_id = local.this.project_id
}


